**INFO: Aplicação em Wordpress**

* *URL Homologação:* <http://vw-ofertasmoustache-hml.ageriservicos.com.br>
* *Diretório App:* /var/deploy/vw-ofertasmoustache-wordpress-hml
* *Repositório:* git@bitbucket.org:stefannoo/ofertasvolkswagen.git

*Observação:* Na task 'GIT | Baixando Branch...', o parâmetro `become_user` foi alterado para `root`.